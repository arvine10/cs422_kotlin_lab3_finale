package cs.mad.flashcards.adapters

import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import cs.mad.flashcards.R
import cs.mad.flashcards.activities.MainActivity2
import cs.mad.flashcards.entities.Flashcard
import cs.mad.flashcards.entities.FlashcardSet


class FlashcardSetAdapter(flashCardset: List<FlashcardSet>) :
    RecyclerView.Adapter<FlashcardSetAdapter.ViewHolder>() { //place name of adapater class.viewHolder
    private val data =  flashCardset.toMutableList()
    // need to get and store a reference to the data set



    fun addnew(flash_cardSet: FlashcardSet){
        data.add(flash_cardSet)
        notifyItemInserted(data.size)
    }

    /**
     * Provide a reference to the type of views that you are using
     * (custom ViewHolder).
     */
    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val textView: TextView = view.findViewById(R.id.text_view)

        // store view references as properties using findViewById on view
    }

    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        /*
            this is written for you but understand what is happening here
            the layout for an individual item is being inflated
            the inflated layout is passed to view holder for storage

            THE ITEM LAYOUT STILL NEEDS TO BE SETUP IN THE LAYOUT EDITOR
         */
        return ViewHolder(LayoutInflater.from(viewGroup.context).inflate(R.layout.item_flashcard_set, viewGroup, false))
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        val item = data[position]
        viewHolder.textView.text = item.title
        viewHolder.textView.setOnClickListener({
            viewHolder.itemView.context.startActivity(Intent(viewHolder.itemView.context, MainActivity2::class.java))
        })


        /*
            fill the contents of the view using references in view holder and current position in data set

            to launch FlashcardSetDetailActivity ->
            viewHolder.itemView.context.startActivity(Intent(viewHolder.itemView.context, FlashcardSetDetailActivity::class.java))
         */

    }

    override fun getItemCount(): Int {
        // return the size of the data set
        return data.size
    }
}
