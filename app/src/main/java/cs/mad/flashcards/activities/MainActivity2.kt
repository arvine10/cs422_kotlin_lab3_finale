package cs.mad.flashcards.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import androidx.recyclerview.widget.RecyclerView
import cs.mad.flashcards.R
import cs.mad.flashcards.adapters.FlashCardAdapter
import cs.mad.flashcards.adapters.FlashcardSetAdapter
import cs.mad.flashcards.entities.Flashcard
import cs.mad.flashcards.entities.FlashcardSet

class MainActivity2 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main2)
        val recyclerView = findViewById<RecyclerView>(R.id.recylcerView)
        recyclerView?.adapter = FlashCardAdapter(Flashcard.getHardcodedFlashcards())

        val button: Button = findViewById(R.id.add)

        button.setOnClickListener({
            val newflash = Flashcard("new","def")
            (recyclerView?.adapter as FlashCardAdapter).addnew(newflash)

        })

    }
}